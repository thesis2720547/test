const EMOTIONS = {
  ANGER: ["Anger"],
  DISGUST: ["Disgust", "Distress"],
  FEAR: ["Fear", "Horror", "Anxiety", "Distress"],
  HAPPY: [
    "Amusement",
    "Contentment",
    "Ecstasy",
    "Excitement",
    "Joy",
    "Satisfaction",
    "Triumph",
    "Aesthetic Appreciation",
    "Adoration",
    "Love",
    "Interest",
  ],
  NEUTRAL: ["Boredom", "Calmness", "Concentration", "Contemplation"],
  SAD: ["Disappointment", "Distress", "Sadness"],
  SURPRISED: ["Surprise (negative)", "Surprise (positive)", "Awe"],
};

module.exports = { EMOTIONS };
