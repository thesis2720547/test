// const res = require("./surprised.json");
// const { EMOTIONS } = require("../constants");

// const correctEmotions = EMOTIONS.SAD || [];

// const parseSpeechModel = (result) => {
//   try {
//     const predictions = result[0].results.predictions;
//     const topEmotions = predictions.map(prediction => {
//       const prosodyEmotions = prediction.models.prosody.grouped_predictions[0].predictions[0].emotions;
//       const topEmotions = prosodyEmotions.sort(
//         (emotion1, emotion2) => emotion2.score - emotion1.score
//       );
  
//       return topEmotions
//       .slice(0, 5)
//       .map((emotion) => emotion.name);
//     });
//     const count = topEmotions.reduce((prev, entry) => {
//       const hasCorrectEmotion = entry.some(
//         (emotion) => correctEmotions.includes(emotion) && emotion !== "Distress"
//       );
//       if (!hasCorrectEmotion) {
//         return prev + 1;
//       }
//       return prev;
//     }, 0);
//     return count;
//   } catch (err) {
//     console.error(err);
//   }
// };

// parseSpeechModel(res);

const angerResult = require("./anger.json");
const disgustResult = require("./disgust.json");
const fearResult = require("./fear.json");
const happyResult = require("./happy.json");
const neutralResult = require("./neutral.json");
const sadResult = require("./sad.json");
const surprisedResult = require("./surprised.json");
const { EMOTIONS } = require("../constants");

const tests = [
  { label: "Anger", result: angerResult, matches: EMOTIONS.ANGER },
  { label: "Disgust", result: disgustResult, matches: EMOTIONS.DISGUST },
  { label: "Fear", result: fearResult, matches: EMOTIONS.FEAR },
  { label: "Happy", result: happyResult, matches: EMOTIONS.HAPPY },
  { label: "Neutral", result: neutralResult, matches: EMOTIONS.NEUTRAL },
  { label: "Sad", result: sadResult, matches: EMOTIONS.SAD },
  { label: "Surprised", result: surprisedResult, matches: EMOTIONS.SURPRISED },
];

const countTruePositive = (positiveResult, matches) => {
  try {
    const predictions = positiveResult[0].results.predictions;
    const topEmotions = predictions.map((prediction) => {
      const facialExpressions =
        prediction.models.prosody.grouped_predictions[0].predictions[0].emotions;
      const topEmotions = facialExpressions.sort(
        (emotion1, emotion2) => emotion2.score - emotion1.score
      );

      return topEmotions.slice(0, 5).map((emotion) => emotion.name);
    });
    const count = topEmotions.reduce((prev, entry) => {
      const hasCorrectEmotion = entry.some((emotion) =>
        matches.includes(emotion)
      );
      if (hasCorrectEmotion) {
        return prev + 1;
      }
      return prev;
    }, 0);
    return count;
  } catch (err) {
    console.error(err);
    return 0;
  }
};

const countTrueNegative = (negativeResults, matches) => {
  try {
    const total = negativeResults.reduce((prev, result) => {
      const predictions = result[0].results.predictions;
      const topEmotions = predictions.map((prediction) => {
        const facialExpressions =
          prediction.models.prosody.grouped_predictions[0].predictions[0].emotions;
        const topEmotions = facialExpressions.sort(
          (emotion1, emotion2) => emotion2.score - emotion1.score
        );

        return topEmotions.slice(0, 5).map((emotion) => emotion.name);
      });

      // if does not have any emotions inside 'matches', increment count
      // == true negative
      const count = topEmotions.reduce((prev, entry) => {
        const hasMatchedEmotions = entry.some((emotion) =>
          matches.includes(emotion) && emotion !== "Distress"
        );
        if (!hasMatchedEmotions) {
          return prev + 1;
        }
        return prev;
      }, 0);
      return prev + count;
    }, 0);
    return total;
  } catch (err) {
    console.error(err);
    return 0;
  }
};

console.log('======TRUE POSITIVE (Total each: 20)======')
tests.forEach((test) => {
  console.log(test.label, ": ", countTruePositive(test.result, test.matches));
});
console.log('======TRUE NEGATIVE (Total each: 120)======')
tests.forEach(test => {
  console.log(test.label, ": ", countTrueNegative(tests.filter(t => t.label !== test.label).map(t => t.result), test.matches));
});